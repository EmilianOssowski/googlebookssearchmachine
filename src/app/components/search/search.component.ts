import {Component, Directive, HostListener, OnInit} from '@angular/core';
import {BooksService} from '../../services/books.service';


@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
  query = '';
  constructor(private booksService: BooksService) { }

  ngOnInit() {
  }
  searchBooks() {
    this.booksService.getBooks(this.query);
  }

}

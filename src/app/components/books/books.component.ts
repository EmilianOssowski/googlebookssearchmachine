import {Component, HostListener, Input, OnInit} from '@angular/core';
import {BooksService} from '../../services/books.service';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {
  books: Array<any> = [];
  constructor(private booksService: BooksService) { }

  ngOnInit() {
    this.booksService.getBooksObservable().subscribe((res) => {
      this.books = res;
    });
  }
}

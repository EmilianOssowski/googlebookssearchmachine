import {Component, Input, OnInit} from '@angular/core';
import {BookModel} from '../../models/book.model';

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.css']
})
export class BookComponent implements OnInit {
  @Input()
  volume;
  book: BookModel;
  constructor() { }

  ngOnInit() {
    this.book = new BookModel(this.volume.volumeInfo.id, this.volume.volumeInfo.title);
    try {
      this.book.setThumnail(this.volume.volumeInfo.imageLinks.thumbnail);
    } catch (e) {
      this.book.setThumnail(null);
    }
    try {
      this.book.setDescription(this.volume.volumeInfo.description);
    } catch (e) {
      this.book.setThumnail(null);
    }
  }
}


export class BookModel {
  id;
  title;
  thumbnail;
  description;
  constructor(id, title) {
    this.id = id;
    this.title = title;
  }
  setThumnail(thumbnail) {
    this.thumbnail = thumbnail;
  }
  setDescription(description) {
    this.description = description;
  }


}

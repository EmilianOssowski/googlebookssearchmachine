import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  url = 'https://www.googleapis.com/';
  booksEndpoint = 'books/v1/volumes';
  constructor(private http: HttpClient) { }
  getBooks(query: string) {
    return this.http.get(this.url + this.booksEndpoint, {params : new HttpParams().set('q', 'intitle:' + query)});
  }
  getNextPageBooks(query: string, num: number) {
    return this.http.get(this.url + this.booksEndpoint, {params : new HttpParams().set('q', 'intitle:' + query
        + '&startIndex' + num * 10)});
  }
}

import { Injectable } from '@angular/core';
import {HttpService} from './http.service';
import {BehaviorSubject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BooksService {
  page: number
  booksList: BehaviorSubject<any> = new BehaviorSubject<any>([]);
  constructor(private http: HttpService) {  }
  getBooks(query: string) {
    this.page = 0;
    this.http.getBooks(query).subscribe((res: BooksResponse) => {
      const volumesList = [];
      const volumesCount = res.totalItems;
      res.items.forEach(volume => {
        volumesList.push(volume);
      });
      this.booksList.next(volumesList);
    });
  }
  getBooksObservable() {
    return this.booksList;
  }

  getNextPageBooks(query: string) {
    this.page = this.page + 1;
    this.http.getNextPageBooks(query, this.page).subscribe((res: BooksResponse) => {
      const volumesList = this.booksList.getValue();
      const volumesCount = res.totalItems;
      res.items.forEach(volume => {
        volumesList.push(volume);
      });
      this.booksList.next(volumesList);
    });

  }
}
export interface BooksResponse {
  totalItems: number;
  items: Array<object>;

}
